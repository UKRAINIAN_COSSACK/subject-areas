﻿--TEST_1

--First
EXPLAIN ANALYZE SELECT * FROM aviatickets 
WHERE aviatickets.id_airlines=5 or aviatickets.id_tours=5

--Second
EXPLAIN ANALYZE SELECT * FROM aviatickets 
WHERE financial_report.service_price >= 1500 AND financial_report.service_price <= 20000 AND 
financial_report.client_paid <= 10000

  