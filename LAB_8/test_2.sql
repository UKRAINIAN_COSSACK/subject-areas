﻿--TEST_2

--Indexes
DROP INDEX financial_report_service_index;
DROP INDEX financial_report_client_index;
DROP INDEX breach_client_index;
DROP INDEX breach_fine_index;

CREATE INDEX breach_client_index ON breach(id_client);
CREATE INDEX breach_fine_index ON breach(fine);

CREATE INDEX financial_report_service_index ON financial_report(service_price);
CREATE INDEX financial_report_client_index ON financial_report(client_paid);

--Analyse_2
--First
EXPLAIN ANALYZE SELECT * FROM aviatickets 
WHERE financial_report.service_price >= 1500 AND financial_report.service_price <= 20000 AND 
financial_report.client_paid <= 10000 

--Second
EXPLAIN ANALYZE SELECT * FROM breach
WHERE breach.id_client=5 OR breach.id_client=7 AND breach.fine >= 1500