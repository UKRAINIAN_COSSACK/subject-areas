�.�. #4

���� ������:
   ��������� ������ ������ � ������������� ����� ����������� �� ��� SQL.

���������� ������:
   1. ��������� ���� ������� ��������� ������ (�. 1.3 � ��. � 1), ��������� ����� ������-����.
      ���������������� ����� ��������������� ���� ����������� SQL
      (INNER / OUTER JOIN, UNION, WITH, GROUP BY, ORDER BY, HAVING, WHERE).
      ���������� �������� � Transact.txt.
   2. ���������� �. 1 �������������� SQL (��� ���������� ������ ���������� ����� ���� � �����)


ճ� ��������� ������:
   ���� �� ���, ���� ��������� ���� ��������� ������ � ����������� ������ �1. ��������� ������ 
   ����� ����������� ������ � ������������� ����� ����������� SQL.

 1 - ���� �볺��� �� ������� ���������: �� ��������, ����� ����������, e-mail �����;
	SELECT surname, town, passport_data, mail
   		FROM "client"; 
 
 2 - ���� ��������� �� ������� ���������: ����� ��� ���������� �볺��� (�������������� WHERE):
	SELECT id_ticket, id_client, movie_id, flight_number
		FROM "aviatickets"
        	WHERE id_client = '1' OR id_client = '3';

 3 - ��������� �������� ������� �볺��� (�������������� ��������� ������� COUNT):
	SELECT COUNT(DISTINCT client)
		FROM "client";

 4 - ��������� �������� ���������� ��� �������������� �������� (�������������� INNER/OUTER JOIN):
	SELECT * FROM "client" 
        	INNER JOIN "breach"
        	ON "client".id_client = "breach".id_client;

 5 - ��������� ���������� ��� ��������� ����� �� ��������� �볺����� ������� ���������� 
     (�������������� GROUP BY �� ORDER BY):
	SELECT SUM(airtickets_price), ID_client FROM "financial_report"
		GROUP BY ID_client ORDER BY SUM(airtickets_price);

 6 - ������ ��������� ����� �� ������, ������� �볺����(�������������� ��������� ������� MAX):
	SELECT MAX(Income) FROM "financial_report";

 7 - ��������� �񳺿 ���������� ��� ���� �� �������� �����(�������������� LEFT JOIN):
	SELECT * FROM "tours" 
		LEFT JOIN "hotel"
		ON "tours".id_tours = "hotel".id_tours;

 8 - ��������� ����� ���������� �� �볺���� ������� ������ �������������� �� ����� '�'
     (�������������� �������� LIKE);
	SELECT * FROM "client"
		WHERE Surname LIKE '�%'; 

 9 - ������� ������� � ��������� �����;
	SELECT id_service, id_service_type FROM "services" WHERE id_service_type = (
        	SELECT MIN(price_service) FROM "service_type" );

10 - ��'������� ���� ������ �� ��������� UNION.

        SELECT ID_type_room, type_name
		FROM "type_room"
		WHERE Capacity = '3'
		UNION
	SELECT ID_type_room, residence_period
		FROM "hotel_number" 
      
   ����� �������� ���������� ������ ������������ � Transact.txt.

��������:
   �� ��� ��������� ���� ����������� ������ ���� �������� ������ ��������� ������ � �������������
   ����� ����������� �� ��� SQL.
