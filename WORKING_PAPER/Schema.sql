﻿DROP TABLE IF EXISTS AIRCRAFT CASCADE;
DROP TABLE IF EXISTS TYPE_ROOM CASCADE;
DROP TABLE IF EXISTS TYPE_VISUM CASCADE;
DROP TABLE IF EXISTS COUNTRY CASCADE;
DROP TABLE IF EXISTS SERVICE_TYPE CASCADE;
DROP TABLE IF EXISTS CITY CASCADE;
DROP TABLE IF EXISTS HOTEL CASCADE;
DROP TABLE IF EXISTS EMBASSY CASCADE;
DROP TABLE IF EXISTS TOURS CASCADE;
DROP TABLE IF EXISTS CLIENT CASCADE;
DROP TABLE IF EXISTS FINANCIAL_REPORT CASCADE;
DROP TABLE IF EXISTS AIRLINE CASCADE;
DROP TABLE IF EXISTS FOOD_CATALOG CASCADE;
DROP TABLE IF EXISTS KLIENT_TOUR CASCADE;
DROP TABLE IF EXISTS HOTEL_NUMBER CASCADE;
DROP TABLE IF EXISTS ARRIVAL_SCHEDULE CASCADE;
DROP TABLE IF EXISTS AVIATICKETS CASCADE;
DROP TABLE IF EXISTS VISUM CASCADE;
DROP TABLE IF EXISTS VISUM_EMBASSY CASCADE;
DROP TABLE IF EXISTS BREACH CASCADE;
DROP TABLE IF EXISTS HOTEL_NUMBER_BREACH CASCADE;
DROP TABLE IF EXISTS SERVICES CASCADE;

CREATE TABLE AIRCRAFT (
	ID_aircraft		SERIAL  		PRIMARY KEY,						
	Released 		DATE			NOT NULL,										
	Reliability		VARCHAR(100) 		DEFAULT NULL					
);

CREATE TABLE TYPE_ROOM (
	ID_Type_room 		SERIAL  		PRIMARY KEY,
	Type_name 		VARCHAR 		NOT NULL,
	Capacity 		INTEGER 		NOT NULL,
	Manning 		VARCHAR(100) 		NOT NULL
);

CREATE TABLE TYPE_VISUM (
	ID_Type_room 		SERIAL  		PRIMARY KEY,
	Type_name 		VARCHAR(30)		NOT NULL,
	Term 			VARCHAR(30)		NOT NULL
);

CREATE TABLE COUNTRY (
	ID_countries 		SERIAL  		PRIMARY KEY,
	Country_Name 		VARCHAR(30) 		NOT NULL
);


CREATE TABLE SERVICE_TYPE (
	ID_service_type 	SERIAL	 		PRIMARY KEY,						
	Service_Name		VARCHAR(30)		NOT NULL,	
	Price_service 		NUMERIC(10,2)		NOT NULL,						

	CHECK ((Price_service >= 0) AND (Price_service <= 200) OR (Service_Name = 'City tours')),		
	CHECK ((Price_service >= 0) AND (Price_service <= 100) OR (Service_Name = 'Excursions to museums')), 	
	CHECK ((Price_service >= 0) AND (Price_service <= 150) OR (Service_Name = 'Boat trips')),		
	CHECK ((Price_service >= 0) AND (Price_service <= 300) OR (Service_Name = 'Waterpark')),		
	CHECK ((Price_service >= 0) AND (Price_service <= 200) OR (Service_Name = 'Hike'))			
);

CREATE TABLE CITY (
	ID_city 		SERIAL  		PRIMARY KEY,
	ID_countries 		INTEGER 		REFERENCES country ON DELETE CASCADE,
	City_name 		VARCHAR(30)		NOT NULL
);

CREATE TABLE HOTEL (
	ID_hotel		SERIAL  		PRIMARY KEY,
	ID_city 		INTEGER		REFERENCES city ON DELETE CASCADE,	
	Hotel_Name		VARCHAR 		NOT NULL,
	Hotel_class 		VARCHAR 		NOT NULL
);

CREATE TABLE EMBASSY (
	ID_embassy 		SERIAL  		PRIMARY KEY,						
	ID_countries		INTEGER 		REFERENCES country ON DELETE CASCADE,						
	Monday	 		VARCHAR(60)		NOT NULL,
	Tuesday			VARCHAR(60)		NOT NULL,
	Wednesday		VARCHAR(60)		NOT NULL,
	Thursday		VARCHAR(60)		NOT NULL,
	Friday			VARCHAR(60)		NOT NULL,
	Start_work 		TIME			NOT NULL,
	End_work 		TIME			NOT NULL,				
	Street 			VARCHAR(60) 		NOT NULL,		 				
	Нouse 			VARCHAR(20) 		NOT NULL,						
	Phone 			VARCHAR(30) 		NOT NULL				
);

CREATE TABLE TOURS (
	ID_tours		SERIAL	 		PRIMARY KEY,
	ID_hotel		INTEGER 		REFERENCES hotel ON DELETE CASCADE,
	ID_city			INTEGER 		REFERENCES city ON DELETE CASCADE,
	ID_countries		INTEGER 		REFERENCES country ON DELETE CASCADE,										
	Term_holidays 		VARCHAR(40)		DEFAULT NULL,			
	Price 			NUMERIC(10,2) 		NOT NULL,						
	Notes 			TEXT 			DEFAULT NULL					
);

CREATE TABLE CLIENT (
	ID_client 		SERIAL	 		PRIMARY KEY,						
	Surname 		VARCHAR(30) 		NOT NULL,						
	First_Name 		VARCHAR(30) 		NOT NULL,						
	Middle_Name 		VARCHAR(30) 		NOT NULL,						
	Town 			VARCHAR(30)		NOT NULL,						
	Street 			VARCHAR(30) 		NOT NULL,						
	House 			INTEGER 		NOT NULL,						
	Apartment 		INTEGER 		NOT NULL,						
	Criminal_history 	VARCHAR(300) 		DEFAULT NULL,		
	Medical_history 	VARCHAR(300) 		NOT NULL,						
	Birth_date 		VARCHAR(30)		NOT NULL,						
	Passport_Data 		VARCHAR(50) 		NOT NULL,						
	Home_phone 		VARCHAR(30) 		DEFAULT NULL,				
	Mobile_phone 		VARCHAR(30) 		NOT NULL,						
	mail 			VARCHAR(30) 		DEFAULT NULL									
);

CREATE TABLE FINANCIAL_REPORT (
	ID_report 		SERIAL	 		PRIMARY KEY, 						
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	Airtickets_price 	NUMERIC(10,2)		NOT NULL,						
	Service_price 		NUMERIC(10,2) 		NOT NULL,						
	Hotel_price 		NUMERIC(10,2) 		NOT NULL,						
	Fines 			NUMERIC(10,2) 		DEFAULT 0,						
	Representative_costs 	NUMERIC(10,2) 		NOT NULL,						
	Client_paid 		NUMERIC(10,2) 		NOT NULL,						
	Income 			NUMERIC(10,2) 		NOT NULL						

	CHECK ((Airtickets_price>= 0) AND (Airtickets_price <= 3000)),						
	CHECK ((Service_price >= 0) AND (Service_price <= 1000)),						
	CHECK ((Hotel_price >= 0) AND (Hotel_price <= 1500)),							
	CHECK ((Representative_costs >= 0) AND (Representative_costs <= 999)),					
	CHECK (Income >= 1000)											
);

CREATE TABLE AIRLINE (
	ID_airlines 		SERIAL  		PRIMARY KEY,
	Airline_name		VARCHAR(30)		NOT NULL,					 	
	ID_aircraft 		INTEGER			REFERENCES aircraft ON DELETE CASCADE											
);

CREATE TABLE FOOD_CATALOG (
	ID_catalog 		SERIAL	 		PRIMARY KEY, 						
	ID_tours		INTEGER 		REFERENCES tours ON DELETE CASCADE,			
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	Breakfast_1 		VARCHAR(300) 		NOT NULL,						
	Breakfast_2 		VARCHAR(300) 		NOT NULL,						
	Dinner 			VARCHAR(300) 		NOT NULL,						
	Lunch 			VARCHAR(300) 		NOT NULL,						
	Supper 			VARCHAR(300) 		NOT NULL						
);

CREATE TABLE KLIENT_TOUR (
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	ID_tours		INTEGER 		REFERENCES tours ON DELETE CASCADE			
);

CREATE TABLE HOTEL_NUMBER (
	ID_rooms 		SERIAL	 		PRIMARY KEY,						
	ID_tours 		INTEGER 		REFERENCES tours ON DELETE CASCADE,			
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,
	ID_Type_room 		INTEGER 		REFERENCES type_room ON DELETE CASCADE,			
	Room_floor 		INTEGER 		NOT NULL,						
	Room_number 		INTEGER 		NOT NULL,						
	Capacity 		INTEGER 		NOT NULL,																
	Manning 		VARCHAR(300) 		NOT NULL,						
	Residence_period 	VARCHAR(40) 		NOT NULL						
);

CREATE TABLE ARRIVAL_SCHEDULE (
	ID_graphics 		SERIAL	 		PRIMARY KEY,						
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	Arrival_time 		TIME	  		NOT NULL,						
	Departure_time 		TIME	 		NOT NULL,
	Arrival_date 		DATE	  		NOT NULL,
	Departure_date		DATE			NOT NULL	
);

CREATE TABLE AVIATICKETS (
	ID_ticket 		SERIAL			PRIMARY KEY,						
	ID_airlines 		INTEGER 		REFERENCES airline ON DELETE CASCADE,			
	ID_graphics 		INTEGER 		REFERENCES arrival_schedule ON DELETE CASCADE,		
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	ID_tours 		INTEGER 		REFERENCES tours ON DELETE CASCADE,			
	Flight_number 		VARCHAR(40) 		NOT NULL,						
	Transplant 		VARCHAR(30) 		DEFAULT 'Direct connections'				
);

CREATE TABLE VISUM (
	ID_visum 		SERIAL	 		PRIMARY KEY,						
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,
	ID_type_visum		INTEGER 		REFERENCES type_visum ON DELETE CASCADE,	
	Price_visum 		NUMERIC(10,2) 		NOT NULL,												

	CHECK ((Price_visum >= 0) AND (Price_visum<= 3000))												
);

CREATE TABLE VISUM_EMBASSY (
	ID_embassy 		INTEGER 		REFERENCES embassy ON DELETE CASCADE,			
	ID_visum 		INTEGER 		REFERENCES visum ON DELETE CASCADE			
);

CREATE TABLE BREACH (
	ID_breach 		SERIAL	 		PRIMARY KEY,						
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	Type_breach 		VARCHAR(100)		NOT NULL,								
	Fine 			NUMERIC(10,2) 		NOT NULL,												
	Notes 			TEXT 			DEFAULT NULL,			

	CHECK ((Fine >= 0) AND (Fine <= 1000))			
);


CREATE TABLE HOTEL_NUMBER_BREACH (
	ID_rooms 		INTEGER 		REFERENCES hotel_number ON DELETE CASCADE,		
	ID_breach 		INTEGER 		REFERENCES breach ON DELETE CASCADE			
);


CREATE TABLE SERVICES (
	ID_service 		SERIAL	 		PRIMARY KEY,						
	ID_client 		INTEGER 		REFERENCES client ON DELETE CASCADE,			
	ID_service_type 	INTEGER 		REFERENCES service_type ON DELETE CASCADE,					
	ID_tours		INTEGER 		REFERENCES tours ON DELETE CASCADE,			
	Notes 			TEXT 			DEFAULT NULL									
);

CREATE OR REPLACE FUNCTION tours_check() RETURNS trigger AS $tours_check$
DECLARE
    BEGIN
	IF NEW.ID_tours IS NULL THEN
            RAISE EXCEPTION 'ID_tours cannot be null';
        END IF;
        IF NEW.ID_Term_holidays IS NULL THEN
            RAISE EXCEPTION 'Term_holidays cannot be null';
        END IF;
        IF NEW.Price IS NULL THEN
            RAISE EXCEPTION '% cannot have null Price ';
        END IF;
        IF (SELECT ID_hotel FROM HOTEL WHERE ID_hotel = NEW.ID_hotel) IS NULL THEN
            RAISE EXCEPTION 'this foreign key ID_hotel does not exist';
        END IF;
        IF (SELECT ID_city FROM CITY WHERE ID_city = NEW.ID_city) IS NULL THEN
            RAISE EXCEPTION 'this foreign key ID_city does not exist';
        END IF;
        IF (SELECT ID_countries FROM COUNTRY WHERE ID_countries = NEW.ID_countries) IS NULL THEN
            RAISE EXCEPTION 'this foreign key ID_countries does not exist';
        END IF;
        RETURN NEW;
    END;
$tours_check$ LANGUAGE plpgsql;

CREATE TRIGGER tours_check BEFORE INSERT OR UPDATE ON TOURS
    FOR EACH ROW EXECUTE PROCEDURE tours_check();
    
--IMPLEMENTATION OF VIEWS
CREATE VIEW clinet_income_vw
	AS SELECT a.id_client, a.surname, a.first_name, a.middle_name, a.home_phone, a.mobile_phone, a.mail,
		  b.id_report, b.airtickets_price, b.service_price, b.hotel_price, b.fines, b.representative_costs, b.client_paid, b.income 
		FROM CLIENT a, FINANCIAL_REPORT b
			WHERE a.id_client = b.id_client
			AND b.income>=2000;
--RSTRICTING ACCESS			
GRANT SELECT ON clinet_income_vw TO BD_admin, Logist, Administrator;

--INSERT
CREATE RULE clinet_income_in AS ON INSERT TO clinet_income_vw
        DO INSTEAD (
        INSERT INTO FINANCIAL_REPORT (id_client, airtickets_price, service_price, hotel_price, fines, representative_costs, client_paid, income)
        VALUES (NEW.id_report, NEW.airtickets_price, NEW.service_price, NEW.hotel_price, NEW.fines, NEW.representative_costs, NEW.client_paid, NEW.income);    
        );

--UPDATE 
CREATE RULE clinet_income_up AS ON UPDATE TO clinet_income_vw 
	DO INSTEAD (
        UPDATE FINANCIAL_REPORT
        SET airtickets_price=NEW.airtickets_price, service_price=NEW.service_price, hotel_price=NEW.hotel_price, fines=NEW.fines, 
            representative_costs=NEW.representative_costs, client_paid=NEW.client_paid, income=NEW.income 
                WHERE id_report= OLD.id_report;
       );
         
--DELETE 
CREATE OR REPLACE RULE clinet_income_del AS ON DELETE TO clinet_income_vw
	DO INSTEAD (
	DELETE FROM FINANCIAL_REPORT 
		WHERE id_report = OLD.id_report;
	DELETE FROM CLIENT 
		WHERE id_client = OLD.id_client;
	);

--TRIGGER
CREATE OR REPLACE FUNCTION clinet_income()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
        IF res = 'INSERT' THEN
		INSERT INTO FINANCIAL_REPORT (id_client, airtickets_price, service_price, hotel_price, fines, representative_costs, client_paid, income)
		VALUES (NEW.id_report, NEW.airtickets_price, NEW.service_price, NEW.hotel_price, NEW.fines, NEW.representative_costs, NEW.client_paid, NEW.income);
		RETURN NEW;
        
	ELSIF res = 'UPDATE' THEN
		UPDATE FINANCIAL_REPORT
		SET airtickets_price=NEW.airtickets_price, service_price=NEW.service_price, hotel_price=NEW.hotel_price, fines=NEW.fines, 
			representative_costs=NEW.representative_costs, client_paid=NEW.client_paid, income=NEW.income 
		WHERE id_report= OLD.id_report;
		RETURN NEW;
        
        ELSIF res = 'DELETE' THEN
                DELETE FROM FINANCIAL_REPORT 
		WHERE id_report = OLD.id_report;
	DELETE FROM CLIENT 
		WHERE id_client = OLD.id_client;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER clinet_income
    INSTEAD OF INSERT OR UPDATE OR DELETE ON clinet_income_vw 
    FOR EACH ROW EXECUTE PROCEDURE clinet_income();