﻿INSERT INTO client (ID_client, Surname, First_Name, Middle_Name, Town, Street, House, Apartment, Criminal_history, Medical_history, Birth_date, Passport_Data, Home_phone, Mobile_phone, mail) 
VALUES ( 3, 'Romanenko', 'Roman', 'Petrovuch', 'Chernigiv', 'yl.Pushkina', 5, 40, 'Do sydy ne prutiagyvavsia!', 'Zdorovui', '27.03.1993', 'HM 100239', '(0021)3-10-44', '(097)3385512', 'reka@mail.com' ), --38
       ( 4, 'Petruk', 'Valentun', 'Igorovuch', 'Dnipropetrovsk', 'yl.Petra I', 6, 134, 'Do sydy ne prutiagyvavsia!', 'Zdorva', '05.07.1975', 'HM 788789', '(0021)3-55-77', '(097)3385552', 'gag.ay@gmail.com' );--38

INSERT INTO embassy (ID_embassy, Country, Work_Days, Work_Hours, Street, Нouse, Phone)
VALUES ( 3, 'Switzerland', 'Monday-Wednesday-Friday-Tuesday-Thursday', 'From 9.00 till 17.00', 'Chornogo', 'byd.34', '(096)5432456');

INSERT INTO  financial_report (ID_report, ID_client, Airtickets_price, Service_price, Hotel_price, Fines, Representative_costs, Client_paid, Income)
VALUES ( 3, 1, 5996.9, 2000, 1500, 30, 300, 8000, 3085.5), --49
       ( 4, 2, 2500, 3455, 1400, 30, 700.5, 7000, 2479.5); --49

INSERT INTO  visum (ID_visum, ID_client, Price_visum, Type_Visum, Term)
VALUES ( 2, 2, 3550, 'Tourist', '3 mis.');  --89

INSERT INTO  breach (ID_breach, ID_client, Type_breach, ID_rooms, Fine, Hotel_Name, Notes)
VALUES ( 1, 1, 'Kurinnya v hromadskomu mistsi', 1, 1001.1 , 'EVA', 'No further information');  --100, 101

INSERT INTO  services (ID_service, ID_client, Kind_Service, ID_city, Price_service)
VALUES ( 2, 2, 'Hike', 1, 100);  --113