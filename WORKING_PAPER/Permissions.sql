﻿--create roles
CREATE ROLE BD_admin WITH SUPERUSER CREATEROLE CREATEDB LOGIN;
CREATE ROLE Administrator WITH LOGIN;
CREATE ROLE Client WITH LOGIN;
CREATE ROLE Logist WITH LOGIN;

GRANT CONNECT ON DATABASE travel_agency TO Client;

--grants for Administrator
GRANT SELECT, UPDATE ON ALL TABLES IN SCHEMA public TO Administrator;

--grants for Client
GRANT SELECT ON aircraft, type_room, country, service_type, city, hotel, embassy, airline TO Client;

--grants for Logist 
GRANT SELECT, UPDATE ON service_type, financial_report TO Logist;
GRANT SELECT ON type_room, country, city, hotel, tours, client, breach TO Logist;