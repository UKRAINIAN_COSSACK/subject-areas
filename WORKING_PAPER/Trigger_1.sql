﻿CREATE TABLE TOURS (
	ID_tours		SERIAL,	 		
	ID_hotel		INTEGER, 		
	ID_city			INTEGER, 		
	ID_countries		INTEGER, 											
	Term_holidays 		VARCHAR(40),					
	Price 			NUMERIC(10,2) 												
);

CREATE OR REPLACE FUNCTION tours_check() RETURNS trigger AS $tours_check$
DECLARE
    BEGIN
	IF NEW.ID_tours IS NULL THEN
            RAISE EXCEPTION 'ID_tours cannot be null';
        END IF;
        IF NEW.ID_Term_holidays IS NULL THEN
            RAISE EXCEPTION 'Term_holidays cannot be null';
        END IF;
        IF NEW.Price IS NULL THEN
            RAISE EXCEPTION '% cannot have null Price ';
        END IF;
        IF (SELECT ID_hotel FROM "HOTEL" WHERE ID_hotel = NEW.ID_hotel) IS NULL THEN
            RAISE EXCEPTION 'this foreign key ID_hotel does not exist';
        END IF;
        IF (SELECT ID_city FROM "CITY" WHERE ID_city = NEW.ID_city) IS NULL THEN
            RAISE EXCEPTION 'this foreign key ID_city does not exist';
        END IF;
        IF (SELECT ID_countries FROM "COUNTRY" WHERE ID_countries = NEW.ID_countries) IS NULL THEN
            RAISE EXCEPTION 'this foreign key ID_countries does not exist';
        END IF;
        RETURN NEW;
    END;
$tours_check$ LANGUAGE plpgsql;

CREATE TRIGGER tours_check BEFORE INSERT OR UPDATE ON TOURS
    FOR EACH ROW EXECUTE PROCEDURE tours_check();