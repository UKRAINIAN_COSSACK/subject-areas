﻿INSERT INTO client (ID_client, Surname, First_Name, Middle_Name, Town, Street, House, Apartment, Criminal_history, Medical_history, Birth_date, Passport_Data, Home_phone, Mobile_phone, mail) 
VALUES ( 1, 'Ivanov', 'Roman', 'Antonovuch', 'Kiev', 'yl.Pushkina', 5, 40, 'Do sydy ne prutiagyvavsia!', 'Zdorovui', '27.03.1993', 'HM 100239', '(00213)3-10-44', '(097)3385512', 'reka@mail.com' ),
       ( 2, 'Gaga', 'Irina', 'Sergeevna', 'Kiev', 'yl.Petra I', 6, 134, 'Do sydy ne prutiagyvavsia!', 'Zdorva', '05.07.1975', 'HM 788789', '(00213)3-55-77', '(097)3385552', 'gag.ay@gmail.com' ),
       ( 3, 'Lapina', 'Irina', 'Vasulivha', 'Lviv', 'yl.Vtlukogo ', 13, 4, 'Do sydy ne prutiagyvavsia!', 'Zdorva', '11.09.1986', 'HM 009890', '(00443)3-44-77', '(097)4343432', 'lapina@gmail.com' ),
       ( 4, 'Zalozny', 'Mihail', 'Vitalievich', 'Kiev', 'yl.Arni ', 4, 13, 'Do sydy ne prutiagyvavsia!', 'Zdorvui', '27.03.1993', 'HM 111111', '(00443)3-10-54', '(097)4495594', 'mihail.zalozny@gmail.com' );
        
INSERT INTO airline (ID_airlines, Plane, Released , Service, Reliability)
VALUES ( 1, 'Boeing 737 JT8D', '2001', 'Ves I klas', 'Na 507 500 lotnikh godin - odna aviakatastrofa' ),
       ( 2, 'Ил-76', '2007', 'I-II klas', 'Na 549 900 lotnikh godin - odna aviakatastrofa'),
       ( 3, 'Airbus A310', '1999', 'Ves-I klas', 'Na 1 067 700 lotnikh godin - odna aviakatastrofa');

INSERT INTO embassy (ID_embassy, Country, Work_Days, Work_Hours, Street, Нouse, Phone)
VALUES ( 1, 'Switzerland', 'Monday-Wednesday-Friday', 'From 9.00 till 17.00', 'Chornogo', 'byd.34', '(096)5432456'),
       ( 2, 'Spain', 'Tuesday-Thursday', 'From 10.00 till 18.00', 'Lermantova', 'byd.6', '(055)8890909');

INSERT INTO tours (ID_city, Country_name, City_name, Hotel_Name, Term_holidays, Price, Notes)
VALUES ( 1, 'Switzerland', 'Berne', 'OLOLO', '10-14 dniv', 8755.2, 'No further information'),
       ( 2, 'Spain', 'Barcelona', 'Shark', '14 dniv', 10300, 'No further information');

INSERT INTO  financial_report (ID_report, ID_client, Airtickets_price, Service_price, Hotel_price, Fines, Representative_costs, Client_paid, Income)
VALUES ( 1, 1, 2984.5, 100, 1500, 30, 300, 8000, 3085.5),
       ( 2, 2, 1000, 100, 1500, 30, 300, 7000, 4070),
       ( 3, 3, 2984.5, 100, 1500, 30, 300, 8000, 3085.5),
       ( 4, 4, 2500, 100, 1400, 30, 700.5, 7000, 2479.5);

INSERT INTO  food_catalog (ID_catalog, ID_city, ID_client, Breakfast_1, Breakfast_2, Dinner, Lunch, Supper)
VALUES ( 1, 1, 1, 'Tosty z syrom - Yayechnya smazhena v hrinkakh z syrom - Yabluchnyi sik', 'Fruktovyi salat', 'Kartoplyane pyure z maslom - Olivye - Borshch - chay z lymonom', 'Desert Evropa', 'Pelmeni po-domashnomu zi smetanoyu');

INSERT INTO  hotel_number (ID_rooms, ID_city, ID_client, Room_floor, Room_number, Capacity, Type_of_room, View_from_room, Manning, Residence_period)
VALUES ( 1, 2, 1, 4, 445, 2, 'Klas-LYKS', 'Horni vershyny', 'Televizor - kondytsioner - muzychnyi prohravach - bihova dorizhka', '14 dniv'),
       ( 2, 2, 2, 3, 345, 1, 'Klas-LYKS', 'More', 'Televizor - kondytsioner - muzychnyi prohravach - bihova dorizhka', '10 dniv'),
       ( 3, 2, 3, 4, 400, 3, 'Klas-LYKS', 'Horni vershyny', 'Televizor - kondytsioner - muzychnyi prohravach - bihova dorizhka', '11 dniv'),
       ( 4, 2, 4, 1, 123, 1, 'Klas-LYKS', 'More', 'Televizor - kondytsioner - muzychnyi prohravach - bihova dorizhka', '14 dniv');

INSERT INTO  arrival_schedule (ID_graphics, ID_client, Arrival, Departure)
VALUES ( 1, 2, '27.03 o 6.10 god.', '12.03 o 13.00 god.'),
       ( 2, 1, '21.05 o 6.10 god.', '11.03 o 13.00 god.'),
       ( 3, 3, '27.03 o 6.10 god.', '12.03 o 13.00 god.'),
       ( 4, 4, '27.03 o 6.10 god.', '12.03 o 13.00 god.');

INSERT INTO  aviatickets (ID_ticket, ID_airlines, ID_graphics, ID_client, ID_city, Flight_number, Transplant)
VALUES ( 1, 1, 1, 2, 1, 'Reis 144 z spoluchennyam Kiev-Berne', 'Direct connections'),
       ( 2, 2, 2, 1, 2, 'Reis 23 z spoluchennyam Kiev-Barcelona', 'Direct connections'),
       ( 3, 2, 3, 3, 2, 'Reis 23 z spoluchennyam Kiev-Barcelona', 'Direct connections'),
       ( 4, 3, 4, 4, 2, 'Reis 441 z spoluchennyam Kiev-Barcelona', 'Direct connections');

INSERT INTO  visum (ID_visum, ID_client, Price_visum, Type_Visum, Term)
VALUES ( 1, 1, 400, 'Tourist', '3 mis.'),
       ( 2, 2, 500, 'Student', '4 mis.'),
       ( 3, 3, 500, 'Student', '4 mis.'),
       ( 4, 4, 400, 'Tourist', '3 mis.');

INSERT INTO  breach (ID_breach, ID_client, Type_breach, ID_rooms, Fine, Hotel_Name, Notes)
VALUES ( 1, 1, 'Kurinnya v hromadskomu mistsi', 1, 30, 'Montana', 'No further information'),
       ( 2, 2, 'Kurinnya v hromadskomu mistsi', 2, 15.5, 'SEA-sports', 'No further information'),
       ( 3, 3, 'Kurinnya v hromadskomu mistsi', 3, 30, 'SEA-sports', 'No further information'),
       ( 4, 4, 'Kurinnya v hromadskomu mistsi', 4, 0, 'Montana', 'No further information');

INSERT INTO  services (ID_service, ID_client, Kind_Service, ID_city, Price_service)
VALUES ( 1, 1, 'Waterpark', 1, 100),
       ( 2, 2, 'Excursions to museums', 2, 20),
       ( 3, 3, 'Boat trips', 2, 60),
       ( 4, 4, 'Hike', 2, 100);